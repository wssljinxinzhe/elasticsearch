# Elasticsearchとは？
## Elasticsearch は Elastic 社が開発しているオープンソースの全文検索エンジンです。
## 大量のドキュメントから目的の単語を含むドキュメントを高速に抽出することができます。

# なぜ、AsamaでElasticsearchを使う。

## Elasticsearch:
- distributed, scalable,and highly available
- Real-time search and analytics capabilities
- sophisticated Restful api
## Asama:
- ASAMA is using AWS ,so can use amazon elastic search service.


# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^updata
# This is plugin info about kuromoji.

## what is kuromoji???
## Elasticsearch デフォルトで、日本語の全文検索を対応してなくて、kuromoji pluginを入れることで、対応させます。

## kuromoji plugin install.
    sudo bin/elasticsearch-plugin install analysis-kuromoji
## elasticsearchを起動。
    bin/elasticsearch
## 






















## --------------------------------------------------------------------------------------------updata

# 全文検索エンジンとは。
## 全文検索エンジンとは端的に言って沢山の文書から特定の文字列を見つけてくるソフトウェアのことです。full-test searchとも呼ばれています。
## Elasticsearch is Full-Text Search.(googleみたいに。)

# PostgreSQL
## 普通に業務上のdataを保存されている。?????

# ElasticSearch:
## kibanaを使用してデータをカスタムの視覚化とレポートを作成できる。(普通のsqlできない)
## logstashを使って、logdataを収集し、分析したり、できます。そして、よく使うdataをランキングしてくれる。ESにデータを、リアルタイムで、検索、保存できる。（sqlできない。）

# sql/elasticsearch
## 普通にsqlでdataを探す時、dataを全部1行ずつスキャンしなければならない為、パフォマンス的にも、よくないです。
例えば、Select * from user where username like 'jinzhe%'sql文のように、間違えて、検索に、jinzheを入力したら、本当の自分の名前jinxinzheが出てこないです、
入力間違えなくても、一億行のdataがあったら、一億のdataを全部スキャンします。パフォマンス的にありえない。
##
## Elasticsearchだと、探し方、結構複雑で、下のURLで、参考しました。
## https://blog.csdn.net/cyony/article/details/65437708(China site)
## ('jinxinzhe'を'jin','xinzhe','jinxin'みたいに、分けて、indexとして、保存されてるらしい、検索する時、条件に合うもの出すみたいかな。（自分の勝手の考えです。）)


## 一般的に私たちの生活のデータは、2つのタイプに分類された構造化データと非構造化データ。

## 構造化データ：固定形式または有限長のデータ（データベース、メタデータなど）。
## 非構造化データ：フルテキストデータとも呼ばれる非構造化データは、メール、ワードドキュメントなど、無期限に長いデータや固定形式を持たないデータを指します。
## もちろん、3番目の場所がいくつかあります。XML、HTMLなどの半構造化データは、構造化データのニーズに従って処理されると、非構造化データに従って純粋なテキストを抽出することもできます。

## 2つのデータ分類によると、検索は構造化データ検索と非構造化データ検索の2つのタイプにも分類されます。

## 構造化データの場合、通常、リレーショナルデータベース（mysql、oracleなど）のテーブルを格納および検索するか、インデックスを作成できます。
## 非構造化データの場合、つまり、フルテキストデータの検索には、主に2つの方法があります。順次スキャン方式、フルテキスト検索です


## --------------------------------------------------------------------------------------------updata





# How are we using it on asama
## 一般に、データベースのパフォーマンスに影響するのは、2つの問題があります。
- 1つはデータベースの読み取りおよび書き込み操作、
- 1つはデータベースのデータが大きすぎるため、操作が遅くなることです。
## 読み取り操作の一部を減らすためのキャッシュ、および一部の複雑なレポート分析と検索をhadoopおよびelasticsearchに与えることができ、書き込みの同時実行性、読み取りも同時実行性、サブライブラリテーブル、マスタースレーブの読み取りと書き込みの分離、または2つの組み合わせを検討できます同時並行性と適時性を改善する他の方法（PGの大きな同時書き込み、ビッグデータビューはelasticsearchを使用してPGデータと同期して読み取ることができます）は、良い結果を達成できます。
## 検索サーバーとして、ElasticSearchは強力なパフォーマンスの利点があり、人気のあるエンタープライズ検索エンジンです。RESTful Webインターフェースに基づいた分散型マルチユーザー対応フルテキスト検索エンジンを提供します。主にリアルタイムの検索および分析エンジンに使用され、構造化データおよび非構造化データの取得をサポートします。
## たとえば、データベースPostgresデータベースのマスタースレーブ構成を使用する場合、ライブラリは主にデータの分析と検索に使用されます。postgresをマルチテーブル、多次元のフルスケールの検索およびユーザーの動作などの分析に使用すると、貴重なデータをマイニングできるため、パフォーマンスが不可能になります。顧客の要件をタイムリーに満たすため、マイニング分析データベースとしてライブラリのPGの代わりにElasticsearchデータベースを使用できます。使用中

## Elasticsearchを簡単に始める。
### https://www.elastic.co/cn/start

# Elastic Stack とは?
## Elastic Stack は Elasticsearch 関連製品の総称です。
##
|製品名|機能|
|:-----|:-----|
|Elasticsearch|	ドキュメントを保存・検索します。|
|Kibana	|データを可視化します。|
|Logstash|	データソースからデータを取り込み・変換します。|
|Beats	|データソースからデータを取り込みます。|
|X-Pack	|セキュリティ、モニタリング、ウォッチ、レポート、グラフの機能を拡張します。|

## 普通のDatabase呼び方の違い。
##
|普通のDatabase|Elasticsearch|
|:-----|:-----|
|データベース	|インデックス。|
|テーブル|マッピングタイプ|
|カラム（列）|フィールド|
|レコード(行)|ドキュメント|

## Elasticsearch は RESTful インターフェースで操作できます。
## RESTful APIを使うメリット

- URIに規律が生まれることで、APIを利用するサービス開発者が楽になる
- URIに規律が生まれることで、API開発者もURIからソースのどの部分なのかが容易にわかる
- ブラウザのアドレスバーにURIを入力すればリソースが参照できる
- サーバ、クライアント間で何も共有しないことにより、負荷に応じたスケーラビリティが向上する。ステートレス性に値するもので、一番のメリットされている。
- GET、POST、PUT、DELETE等のHTTP標準のメソッドを使うことで、シンプルで一貫性のあるリクエスト標準化が円滑に行える。統一インターフェースに値する。

# 簡単なElasticsearchのDocumentを作成する。
##
	#    +--- Index name
	#    |       +--- Type name
	#    |       |     +--- Document ID
	#    |       |     |
	#    V       V     V
	PUT /library/_doc/1
	{
  	"title": "Norwegian Wood",
  	"name": {
    	"first": "Haruki",
    	"last": "Murakami"
  	},
  	"publish_date": "1987-09-04T00:00:00+0900",
  	"price": 19.95
	}
##
# 作ったDocumentを確認。
##
	GET /library/_doc/1
##
# Updataには。
##
    PUT /library/_doc/1
    {
    "title": "Norwegian Wood",
  	"name": {
    	"first": "Haruki",
    	"last": "Murakami"
  	},
  	"publish_date": "1987-09-04T00:00:00+0900",
  	"price": 29.95
	}
##
# 部分的に更新。
##
	POST /library/_update/1
	{
  	"doc": {
    	"price": 10
  	}
	}
##
# ドキュメントに項目を追加.
##
	POST /library/_update/1
	{
  	"doc": {
    	"price_jpy": 1800
  	}
	}
##
# Documentを削除には。
##
	DELETE /library/_doc/1
##

# 以上の勉強資料を全部下のサイトに載せってあります。
## https://qiita.com/nskydiving/items/1c2dc4e0b9c98d164329
##
# 詳しく勉強するためには、勉強するために、下のサイトから。
### https://www.elastic.co/guide/jp/elasticsearch/reference/current/gs-basic-concepts.html