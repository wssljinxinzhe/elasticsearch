１、データの作成（Documentを指定しない方法）

POST /library/_doc/
{
  "title": "パソナテキーラ金信晢",
  "name": {
    "名前": "金信晢",
    "職業": "プログラマー"
    "電話": "090-1255-6888"
  },
  "publish_date": "2002-09-12T00:00:00+0900"
}

2、データを取得

GET /library/_doc/

3、全搜索
    GET /library/_search